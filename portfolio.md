## 6/12
- Uitleg gevraagd over week 5 oefeningen aan Collega's.
    - Oefeningen 11 tot 17 gemaakt.
- Oefeningen van week 1 tot 5 gepushed.     
    
    
## 5/12
- Slides bekeken van week 1.
- Video's bekeken van week 1.
    - Oefeningen week 1 gemaakt.
- Slides bekeken van week 2.
- Video's bekeken van week 2.
    - Oefeningen week 2 gemaakt.
- Slides bekeken van week 3.
- Video's bekeken van week 3.
- Slides bekeken van week 4.
- Video's bekeken van week 4.
- Slides bekeken van week 5.
- Video's bekeken van week 5.
    - Oefeningen 1 tot 10 gemaakt van week 5.

## 4/12
- Collega's gecontacteert omdat er bij week 3 geen oefeningen of theorie staat.
## 3/12
- Les bijgewoond van week 9.
- Gesproken met mijn groep 
    - Wat is mijn functie in de groep.
    - Wat ik kan bijdragen.
- Met andere leerlingen gesproken in verband met mijn portfolio.md.
    - De layout.
    - Algemene werking.
    - Mappestructuur in GitLab.

## 2/12
- Gesprek met meneer Roeland gehad.
    - Hoe ik te werk moet gaan.
    - Hoe analyse in elkaar zit.
    - Mijn groep.
- Van groep veranderd.
- Mail geschreven naar meneer De Beu inverband dat mijn gitlab account in de klasgroup van gitlab zit.    
- Theorie van week 9 voorbereid voor de komende les.
    - Slides bekeken.
    - Video's bekeken.




## 26/11


## 19/11

## 29/10
- Ingeschreven voor analyse.

